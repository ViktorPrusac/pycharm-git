import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import OneHotEncoder as OHE

param_niter = 500
learning_rate = 0.0035
batch_size = 30


class TFLogreg:
    def __init__(self, D, C, param_delta=0.5):
        self.X = tf.placeholder(tf.float32, [None, D])
        self.Yoh_ = tf.placeholder(tf.float32, [None, C])

        self.a = tf.Variable(tf.zeros([D, C]))
        self.b = tf.Variable(tf.zeros([C]))

        self.model = tf.matmul(self.X, self.a) + self.b
        self.cost = tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.model, labels=self.Yoh_)
        self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=param_delta).minimize(self.cost)

    def train(self, X, Yoh_, param_niter=500):
        init = tf.global_variables_initializer()

        with tf.Session() as sess:
            sess.run(init)

            cost_history, accuracy_history = [], []

            for epoch in range(param_niter):
                cost_per_epoch = 0

                sess.run(self.optimizer, feed_dict={self.X: X, self.Yoh_: Yoh_})

                c = sess.run(self.cost, feed_dict={self.X: X, self.Yoh_: Yoh_})

                correct_prediction = tf.equal(tf.argmax(self.model, 1), tf.argmax(self.Yoh_, 1))
                accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

                cost_history.append(sum(c))
                accuracy_history.append(accuracy.eval({self.X: X, self.Yoh_: Yoh_}) * 100)

                if epoch % 50 == 0 and epoch != 0:
                    print("Epoch " + str(epoch) + " Cost: " + str(cost_history[-1]))

            a = sess.run(self.a)
            b = sess.run(self.b)

            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
            print("\nAccuracy:", accuracy_history[-1], "%")

            return a, b


if __name__ == "__main__":
    np.random.seed(100)
    tf.set_random_seed(100)

    points = pd.read_csv('dataset.csv', header=None)

    x_orig = points.iloc[:, 1:-1].values
    y_orig = points.iloc[:, -1:].values

    oneHot = OHE()

    oneHot.fit(x_orig)
    X = oneHot.transform(x_orig).toarray()

    oneHot.fit(y_orig)
    Yoh_ = oneHot.transform(y_orig).toarray()

    tflr = TFLogreg(X.shape[1], Yoh_.shape[1], learning_rate)
    a, b = tflr.train(X, Yoh_, param_niter)

    x_pos = np.array([x_orig[i] for i in range(len(x_orig)) if y_orig[i] == 1])
    x_neg = np.array([x_orig[i] for i in range(len(x_orig)) if y_orig[i] == 0])

    plt.scatter(x_pos[:, 0], x_pos[:, 1], color='blue')
    plt.scatter(x_neg[:, 0], x_neg[:, 1], color='red')

    X_pred = np.array([np.min(x_orig[:, 0]), np.max(x_orig[:, 0])])
    Y_pred = (- 1.0) * (X_pred * a + b)
    Y_pred = [sum(Y_pred[:, 0]) + 2.75, sum(Y_pred[:, 1]) + 2.75]
    plt.plot(X_pred, Y_pred)
    plt.legend()

    plt.show()
