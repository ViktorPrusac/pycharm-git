import numpy as np
from sklearn.linear_model import LogisticRegression as LR
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
import data

param_niter = 1000
model = LR(max_iter=param_niter)


def binlogreg_train(X, Y_):
    model.fit(X, Y_)
    predicted_classes = model.predict(X)
    accuracy = accuracy_score(Y_.flatten(), predicted_classes)
    parameters = model.coef_

    return parameters


def binlogreg_classify(X):
    return model.predict(X)


def binlogreg_decfun(X):
    def classify(X):
        return binlogreg_classify(X)

    return classify


if __name__ == "__main__":
    np.random.seed(100)

    X, Y_ = data.sample_gauss_2d(2, 100)

    w = binlogreg_train(X, Y_)
    Y = binlogreg_classify(X)

    accuracy, recall, precision = data.eval_perf_binary(Y, Y_)
    AP = data.eval_AP(Y_[Y.argsort()])

    print(accuracy, recall, precision, AP)

    decfun = binlogreg_decfun(X)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)
    data.graph_data(X, Y_, Y, special=[])

    plt.show()
