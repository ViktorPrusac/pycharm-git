import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier

import data

param_niter = 10000  # (1e5)
param_delta = 0.05
param_lambda = 1e-3
K = 6
C = 2
N = 10
model = MLPClassifier(activation='relu', alpha=param_lambda, learning_rate_init=param_delta, max_iter=100000)


def fcann2_train(X, Y_):
    model.fit(X, Y_)
    parameters = model.get_params()
    return parameters


def fcann2_classify(X):
    return model.predict(X)


def fcann2_decfun(X):
    def classify(X):
        return fcann2_classify(X)

    return classify


if __name__ == "__main__":
    np.random.seed(100)

    X, Y_ = data.sample_gmm_2d(K, C, N)

    w = fcann2_train(X, Y_)

    Y = fcann2_classify(X)

    accuracy, recall, precision = data.eval_perf_binary(Y, Y_)
    AP = data.eval_AP(Y_[Y.argsort()])

    print(accuracy, recall, precision, AP)

    decfun = fcann2_decfun(X)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)
    data.graph_data(X, Y_, Y, special=[])

    plt.show()
