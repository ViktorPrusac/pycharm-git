import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

X = tf.placeholder("float")
Y_ = tf.placeholder("float")
a = tf.Variable(np.random.randn(), name="a")
b = tf.Variable(np.random.randn(), name="b")
learning_rate = 0.01
number_of_points = 50
param_niter = 1000


def tf_linreg_learning(x, y):
    Y = tf.add(tf.multiply(a, X), b)
    loss = tf.reduce_sum(tf.pow(Y - Y_, 2)) / (2 * number_of_points)

    trainer = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)
    init = tf.global_variables_initializer()

    with tf.Session() as sess:
        sess.run(init)

        for iter in range(param_niter):

            for (_x, _y) in zip(x, y):
                sess.run(trainer, feed_dict={X: _x, Y_: _y})

            if (iter + 1) % 50 == 0:
                cost = sess.run(loss, feed_dict={X: x, Y_: y})
                print("Iteration", (iter + 1), ": cost =", cost, "a =", sess.run(a), "b =", sess.run(b))

        training_cost = sess.run(loss, feed_dict={X: x, Y_: y})
        weight = sess.run(a)
        bias = sess.run(b)

    return weight, bias


if __name__ == "__main__":
    np.random.seed(100)
    tf.set_random_seed(100)

    x = np.linspace(0, 50, number_of_points)
    y = np.linspace(0, 50, number_of_points)

    x += np.random.uniform(-4, 4, number_of_points)
    y += np.random.uniform(-4, 4, number_of_points)

    a, b = tf_linreg_learning(x, y)
    y_pred = a * x + b

    plt.plot(x, y, 'ro', label='Original data')
    plt.plot(x, y_pred, label='Fitted line')
    plt.legend()
    plt.show()
